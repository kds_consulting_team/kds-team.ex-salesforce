Salesforce Extractor component for Keboola Connection.

## Functionality

The component exports data from the Salesforce based on SOQL you provide and save it into the out/tables directory in file named as object.csv.

### Configuration

#### Authentication

**Login Name** - (REQ) your user name, when exporting data from sandbox don't forget to add .sandboxname at the end
**Password** - (REQ) your password
**Security Token** - (REQ) your security token, don't forget it is different for sandbox
**sandbox** - (REQ) true when you want to export data from sandbox


**Object** - Salesforce object identifier, eg. Account.

##### Incremental loading 

Set `incremental` to true to download only new records since the last extraction. The `Id` column must be included if you use custom query as it will be the primary key.

**NOTE**: The extractor uses the `LastModifiedDate` to perform the incremental fetching. Some datasets like `*_history` do not have the `LastModifiedDate` column defined. 

In such cases, specify the increments using the custom query and use the `JSON EDITOR` to setup manifest to control the incremental loads.



**Example:**

```json
{
  "parameters": {
    "sinceLast": false,
    "objects": [
      {
        "name": "CaseHistory",
        "soql": "SELECT Id, CaseId, Field, CreatedById, CreatedDate, IsDeleted, NewValue, OldValue FROM CaseHistory where CreatedDate = LAST_N_DAYS:5"
      }
    ]
  },
  "processors": {
    "after": [
      {
        "definition": {
          "component": "keboola.processor-create-manifest"
        },
        "parameters": {
          "incremental": true,
          "primary_key": [
            "Id"
          ]
        }
      }
    ]
  }
}
```


As of Spring '17 release this connector allow querying for deleted records (use IsDeleted).