package keboola.salesforce.extractor;
/**
 * @author David Esner
 */
public class ExtractionException extends KBCException {

	public ExtractionException(String message, int severity) {
		super(message, severity);
		// TODO Auto-generated constructor stub
	}

	public ExtractionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
