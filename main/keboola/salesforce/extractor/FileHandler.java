package keboola.salesforce.extractor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
 
 
public class FileHandler
{       
    public static void writeCSVFromStream(InputStream in, String object, String filesDirectory, Boolean firstFile) throws IOException{
        //gets QueryResultStream from BulkExample and object name, creates BufferedReader and output file in specified folder
        //makes output folder if it doesnt exist
        try {
	    	BufferedReader read = new BufferedReader(new InputStreamReader(in));
	        //writes csv file to output folder
	
	        BufferedWriter csvFile = new BufferedWriter( new FileWriter(filesDirectory+object+".csv", true));
	        
	        String line = "";
	        Boolean firstLine = true;
//	        CsvWriter writer = new CsvWriter(csvFile);
	        //makes array of results
	        while((line = read.readLine()) != null){
	                if(line.length()>0){
	                        line.trim();
//	                        String[] row = line.split("___");
	                        if( !firstFile && firstLine) { 
	                        } else {
	                        	csvFile.write( line);
		                        csvFile.newLine();
		                        csvFile.flush();
								line = null; // try to null the variable so it can be cleaned, no idea why it consume so much memory
	                        }
//	                        writer.writeRecord(row);
	                }
                    firstLine = false;	           
	        }
	        //closes stream
	   		csvFile.close();
//	        writer.endDocument();
        } catch (Exception e) {
			e.printStackTrace();
			System.err.println( "FileWriter Exception on " + e.getStackTrace()[0].getLineNumber());
//			System.exit(1);
        }
    }
}