FROM maven:3.6.2-jdk-8-slim
MAINTAINER KDS Team <data_ca@keboola.com>

ENV APP_VERSION 1.1.0

# set switch that enables correct JVM memory allocation in containers
ENV MAVEN_OPTS="-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap"
ENV JAVA_OPTS="-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap"

COPY . /code/
WORKDIR /code/

RUN mvn compile

# https://github.com/keboola/docker-bundle/issues/198
RUN chmod a+rw ./ -R
RUN mkdir -p /var/www/.m2 && chmod a+rw /var/www/.m2 -R

# https://github.com/carlossg/docker-maven#running-as-non-root
# ENV MAVEN_CONFIG=/var/maven/.m2

ENTRYPOINT mvn -q -e exec:java -Dexec.args=/data  